#ifndef _FILES_MGM_H_
#define _FILES_MGM_H_

#include <stdio.h>

FILE* input_image_file_open(const char* file_name);
FILE* output_image_file_create(const char* file_name);
void delete_output_image_file(const char* file_name);
void image_file_close(FILE* file);
size_t get_image_file_size(FILE* file);

#endif
