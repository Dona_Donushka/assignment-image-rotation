#ifndef _BMP_HEADER_H_
#define _BMP_HEADER_H_

#include  <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INTERNAL_ERROR
  /* коды других ошибок  */
  };



/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

bool is_bmp_file_valid(FILE* in);
uint32_t get_height(FILE* in);
uint32_t get_width(FILE* in);
uint32_t get_offset(FILE* in);
size_t padding(size_t width,size_t byte_per_pixel);
enum write_status write_header_to_file(FILE* out, uint32_t height, uint32_t width, size_t size_of_pixel);

#endif


