#include "bmp.h"
#include "files_mgm.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>





static void clear_context(FILE* in_file, FILE* out_file, struct image* in_image, struct image* out_image);

static void usage(int argc);

/*
static const char* read_error_text[] = {
    [READ_OK] = "File Read Successfully\n",
    [READ_INVALID_SIGNATURE] = "Invalid BMP Siganture File\n",
    [READ_INVALID_BITS] = "Invalid BMP Data\n",
    [READ_INVALID_HEADER] = "Invalid BMP Header File\n",
    [READ_INTERNAL_ERROR] = "Internal Error\n"
};
*/


int main(int argc, char** argv) {
// 0) Проверить аргументы, не меньше трех элементов мы должны получить
    usage(argc);

    const char* const input_file_name = argv[1];
    const char* const output_file_name = argv[2];

    FILE* source_file = NULL;
    FILE* destiantion_file = NULL;
    struct image image = {0, 0, NULL};
    struct image rotated_image = {0, 0, NULL};

// 1) Открыть файл и считать содержимое
    source_file = input_image_file_open(input_file_name);

    if(!source_file) {
        fprintf(stderr, "File Does Not Exist\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

    const enum read_status read_result = from_bmp( source_file, &image );

    if(read_result != READ_OK){
        //const char* err_text = read_error_text[read_result];
        //fprintf(stderr, err_text);

        switch(read_result){
        	case READ_INVALID_SIGNATURE:
        		fprintf(stderr, "Invalid BMP Siganture File\n");
        		break;
        	case READ_INVALID_BITS:
        		fprintf(stderr, "Invalid BMP Data\n");
        		break;
        	case READ_INVALID_HEADER:
        		fprintf(stderr, "Invalid BMP Header File\n");
        		break;
        	case READ_INTERNAL_ERROR:
        		fprintf(stderr, "Internal Error\n");
        		break;
        	default:
        		fprintf(stderr, "Unknown Error\n");
        		break;
        }

        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

// 2) Трансформация(поворот налево)
    rotated_image = image_turn_left(image);

    if (!rotated_image.data){
        fprintf(stderr, "File Transfomation Error\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

// 3) Записать данные в файл и закрыть файл
    destiantion_file = output_image_file_create(output_file_name);
    const enum write_status write_result = to_bmp( destiantion_file, &rotated_image );

    if(write_result != WRITE_OK) {
        fprintf(stderr, "Can't create file\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        delete_output_image_file(output_file_name);
        exit(1);
    }

    clear_context(source_file, destiantion_file, &image, &rotated_image);

    printf("Transformation Was Successful\n");

    return 0;
}

static void clear_context(FILE* in_file, FILE* out_file, struct image* in_image, struct image* out_image) {
    if (in_file) { image_file_close(in_file); }
    if (out_file) { image_file_close(out_file); }
    image_destroy(in_image);
    image_destroy(out_image);
}

static void usage(int argc){
    if(argc < 3){
        fprintf(stderr, "Usage: bmp_rotate INPUT_FILE OUTPUT_FILE\n");
        exit(1);
    }
}
