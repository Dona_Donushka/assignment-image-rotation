#include "bmp_header.h"
#include "files_mgm.h"

#define BMP_SIGNATURE 0x4D42
#define PADDING 4

struct bmp_header {
    struct __attribute__((packed)) {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bfOffBits;
    } BITMAPFILEHEADER;

    struct __attribute__((packed)) {
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
    } BITMAPINFO;
};


static enum read_status bmp_header_read(FILE* in, struct bmp_header* header);
static struct bmp_header create_header(uint32_t height, uint32_t width, size_t size_of_pixel);

bool is_bmp_file_valid(FILE* in){
  struct bmp_header header = {0};
  return bmp_header_read(in, &header) == READ_OK;
}


uint32_t get_height(FILE* in){
  struct bmp_header header = {0};//создаем header и обнуляем все элементы

  return bmp_header_read(in, &header) == READ_OK
      ? header.BITMAPINFO.biHeight
      : 0;
}

uint32_t get_width(FILE* in){
  struct bmp_header header = {0};//создаем header и обнуляем все элементы

  return bmp_header_read(in, &header) == READ_OK
      ? header.BITMAPINFO.biWidth
      : 0;
}

uint32_t get_offset(FILE* in){
  struct bmp_header header = {0};//создаем header и обнуляем все элементы

  return bmp_header_read(in, &header) == READ_OK
      ? header.BITMAPFILEHEADER.bfOffBits
      : 0;
}

enum write_status write_header_to_file(FILE* out, uint32_t height, uint32_t width, size_t size_of_pixel){
// 1) создать и записать header
  struct bmp_header new_header = create_header(height, width, size_of_pixel);
  size_t ret = fwrite(&new_header, sizeof(struct bmp_header), 1, out);

  return ret == 1
      ?   WRITE_OK
      : WRITE_ERROR;
}

static struct bmp_header create_header(uint32_t height, uint32_t width, size_t size_of_pixel) {
    const size_t size_of_data = (width * size_of_pixel + padding(width, size_of_pixel)) * height;// Размер пиксельных данных в байтах.
    struct bmp_header header = {0};

    header.BITMAPFILEHEADER.bfType = BMP_SIGNATURE;
    header.BITMAPFILEHEADER.bfileSize =  sizeof(struct bmp_header) + size_of_data;
    header.BITMAPFILEHEADER.bfReserved = 0;
    header.BITMAPFILEHEADER.bfOffBits = sizeof(struct bmp_header); // Мы пропускаем Colour table and Gap1, поэотму положение пиксельных данных относительно начала данной структуры будет равно размеру header'а

    header.BITMAPINFO.biSize = sizeof(header.BITMAPINFO);
    header.BITMAPINFO.biWidth = width;
    header.BITMAPINFO.biHeight = height;
    header.BITMAPINFO.biPlanes = 1; // В BMP допустимо только значение 1
    header.BITMAPINFO.biBitCount = size_of_pixel * 8;
    header.BITMAPINFO.biCompression = 0; // Храним в двумерном массиве, поэтому значение 0
    header.BITMAPINFO.biSizeImage = size_of_data; // Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом.
    header.BITMAPINFO.biXPelsPerMeter = 0; //
    header.BITMAPINFO.biYPelsPerMeter = 0; //
    header.BITMAPINFO.biClrUsed = 0; // Таблицы цветов нет
    header.BITMAPINFO.biClrImportant = 0;

    return header;
}

size_t padding(size_t width,size_t byte_per_pixel) {
    size_t line_size = width * byte_per_pixel;

    return line_size % PADDING != 0
        ? PADDING - line_size % PADDING
        : 0;
}

static enum read_status bmp_header_read(FILE* in, struct bmp_header* header){
    size_t ret = fread(header, sizeof(struct bmp_header), 1, in);
    if(ret != 1){ // читаем заголовок из файла и проверяем его на ошибку чтения
        return READ_INVALID_HEADER;
    }

    ret = fseek(in, 0L, SEEK_SET); // вернуть указатель в начало файла
    if (ret != 0) {
        return READ_INTERNAL_ERROR;
    }

    if(header->BITMAPFILEHEADER.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if(header->BITMAPFILEHEADER.bfileSize != get_image_file_size(in)){
        return READ_INVALID_BITS;
    }

    return READ_OK;
}
