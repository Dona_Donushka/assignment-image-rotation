#include "image.h"
#include <stdlib.h>


struct image image_create(size_t width, size_t height){
    struct image img = {0};

    img.data = malloc(height * width * sizeof(struct pixel));

    if(!img.data) {
        return img;
    }

    img.height = height;
    img.width = width;

    return img;
}



void image_destroy(struct image* img){
    free(img->data);
    img->width = 0;
    img->data = NULL;
    img->height = 0;
}



