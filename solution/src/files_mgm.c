#include "files_mgm.h"
#include <stdio.h>
#include <sys/stat.h>


extern int fileno(FILE*); //добавлено из-за ошибки компилятора

FILE* input_image_file_open(const char* file_name) {
    return fopen(file_name, "rb");
}

FILE* output_image_file_create(const char* file_name) {
    return fopen(file_name, "w+b");
}

void image_file_close(FILE* file) {
     fclose(file);
}

size_t get_image_file_size(FILE* file){
    const int fd = fileno(file);
    struct stat buf;
    fstat(fd, &buf);
    return buf.st_size;
}

void delete_output_image_file(const char* file_name) {
    remove(file_name);
}
