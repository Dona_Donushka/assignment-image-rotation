#include "bmp.h"
#include "bmp_header.h"
#include "files_mgm.h"


//static struct bmp_header create_header(const struct image* img);
//static size_t padding(size_t width,size_t byte_per_pixel);
//static enum read_status bmp_header_read(FILE* in, struct bmp_header* header);

enum read_status from_bmp( FILE* in, struct image* img ) {

    if(!is_bmp_file_valid(in)){
        return READ_INVALID_HEADER;
    }

    // Выделяем место в памяти, куда можно будет сохранить картинку
    const uint32_t height = get_height(in);
    const uint32_t width = get_width(in);
    const uint32_t offset = get_offset(in);

    if(!height || !width || !offset){
        return READ_INVALID_HEADER;
    }


//----------------------------------------------------------------------------
    int res = fseek (in, offset, SEEK_SET); // становимся в данное нам значение, и идем дальше по файлику
    if(res != 0){
        return READ_INVALID_HEADER;
    }


    const size_t padding_size = padding(width, sizeof(struct pixel));

    struct image temp_image = image_create(width, height);
    if(!temp_image.data) {
        return READ_INTERNAL_ERROR;
    }

    for(size_t i = 0; i < height; i++){
        size_t rc = fread(temp_image.data + width * i, width * sizeof(struct pixel), 1, in);
        if(rc != 1) {
            image_destroy(&temp_image);
            return READ_INVALID_BITS;
        }
        rc = fseek (in, (long)padding_size, SEEK_CUR);
        if(rc != 0) {
            image_destroy(&temp_image);
            return READ_INVALID_BITS;
        }
    }

    *img = temp_image;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
// 1) создать и записать header
    enum write_status res = write_header_to_file(out, img->height, img->width, sizeof(struct pixel));
    if(res == WRITE_ERROR) return res;

// 2) записать данные(строки) с учетом падинга

    const uint8_t padding_value[3] = {0};
    const size_t padding_size = padding(img->width, sizeof(struct pixel));

    for(size_t i = 0; i < img->height; i++){
        // записываем
        size_t rc = fwrite(img->data + img->width * i, img->width * sizeof(struct pixel), 1, out);
        if(rc < 1) return WRITE_ERROR;

        // записать padding
        rc = fwrite(padding_value, padding_size, 1, out);
        if(padding_size != 0 &&  rc < 1) return WRITE_ERROR;
    }

    return WRITE_OK;
}




